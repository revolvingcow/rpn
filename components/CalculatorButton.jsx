import React from 'react';

import {
    Text,
    TouchableOpacity,
    View
} from 'react-native';

export const CalculatorButton = (props) => {
    const textColor = (props.style || {}).color;
    return (
        <View style={{ padding: 5 }}>
            <TouchableOpacity style={{ ...props.style, borderRadius: 10 }} onPress={props.onPress}>
                <Text style={{ color: textColor }}>{props.title}</Text>
            </TouchableOpacity>
        </View>
    );
};
