import React, { useState } from 'react';
import { Dimensions } from "react-native";
import {
    SafeAreaView,
    StyleSheet,
    View,
    Text,
    FlatList,
} from 'react-native';
import { useTheme } from '@react-navigation/native';
import { CalculatorButton } from '../components/CalculatorButton';

const screenWidth = Dimensions.get("window").width;

const Item = ({ colors, item }) => {
    return (<Text style={styles.registryText}>{item.number}</Text>);
};

export const CalculatorScreen = ({ navigation }) => {
    const { colors } = useTheme();
    const [stack, setStack] = useState([]);
    const [display, setDisplay] = useState('0');

    const number = (n) => {
        const previous = display === '0' ? '' : display
        setDisplay(`${previous}${n}`);
    };

    const enter = () => {
        const i = parseFloat(display);
        if (!isNaN(i)) {
            setStack(stack.concat(i));
            setDisplay('0');
        }
    };

    const clear = () => {
        if (display === '0') {
            setStack([]);
            setDisplay('0');
        } else {
            setDisplay('0');
        }
    };

    const back = () => {
        if (display === '0') {
            if (stack.length) {
                setDisplay(`${stack.pop()}`);
                setStack(stack.concat([]));
            }
            return;
        }

        const next = display.slice(0, -1);
        setDisplay(next || '0');
    };

    const operator = (sym) => {
        switch (sym) {
        case 'sin':
            if (stack.length) {
                const a = stack.pop();
                setStack(stack.concat(Math.sin(a)));
            }
            break;
        case 'cos':
            if (stack.length) {
                const a = stack.pop();
                setStack(stack.concat(Math.cos(a)));
            }
            break;
        case 'tan':
            if (stack.length) {
                const a = stack.pop();
                setStack(stack.concat(Math.tan(a)));
            }
            break;
        case 'log':
            if (stack.length) {
                const a = stack.pop();
                setStack(stack.concat(Math.log(a) / Math.log(10)));
            }
            break;
        case '^':
            if (stack.length > 1) {
                const a = stack.pop();
                const b = stack.pop();
                setStack(stack.concat(Math.pow(b, a)));
            }
            break;
        case '/':
            if (stack.length > 1) {
                const a = stack.pop();
                const b = stack.pop();
                setStack(stack.concat(b / a));
            }
            break;
        case '*':
        case 'x':
            if (stack.length > 1) {
                const a = stack.pop();
                const b = stack.pop();
                setStack(stack.concat(b * a));
            }
            break;
        case '+':
            if (stack.length > 1) {
                const a = stack.pop();
                const b = stack.pop();
                setStack(stack.concat(b + a));
            }
            break;
        case '-':
            if (stack.length > 1) {
                const a = stack.pop();
                const b = stack.pop();
                setStack(stack.concat(b - a));
            }
            break;
        }
    };

    const reverse = (arr) => {
        const a = [];
        for (let i = arr.length - 1; i >= 0; i--) {
            a.push(arr[i]);
        }
        return a;
    };

    const stackList = reverse(stack).map((x, index) => {
        return {
            key: `stack-item-${index}-${x}`,
            number: x,
        };
    });

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.registryRow}>
                <FlatList style={styles.registry}
                          horizontal={true}
                          inverted={true}
                          data={stackList}
                          renderItem={({ item }) => <Item item={item} colors={colors} />}
                          keyExtractor={item => item.key} />
            </View>
            <View style={styles.display}>
                <Text style={{ ...styles.displayText, color: colors.text }}>{display}</Text>
            </View>
            <View style={styles.numPadContainer}>
                <View style={styles.numPad}>
                    <View style={styles.row}>
                        <CalculatorButton style={styles.radianKey} title="sin" onPress={() => operator('sin')} />
                        <CalculatorButton style={styles.radianKey} title="cos" onPress={() => operator('cos')} />
                        <CalculatorButton style={styles.radianKey} title="tan" onPress={() => operator('tan')} />
                    </View>
                    <View style={styles.row}>
                        <CalculatorButton style={styles.clearKey} title="clear" onPress={clear} />
                        <CalculatorButton style={styles.clearKey} title="back" onPress={back} />
                        <CalculatorButton style={styles.operatorKey} title="log" onPress={() => operator('log')} />
                    </View>
                    <View style={styles.row}>
                        <CalculatorButton style={styles.numberKey} title="7" onPress={() => number(7)} />
                        <CalculatorButton style={styles.numberKey} title="8" onPress={() => number(8)} />
                        <CalculatorButton style={styles.numberKey} title="9" onPress={() => number(9)} />
                    </View>
                    <View style={styles.row}>
                        <CalculatorButton style={styles.numberKey} title="4" onPress={() => number(4)} />
                        <CalculatorButton style={styles.numberKey} title="5" onPress={() => number(5)} />
                        <CalculatorButton style={styles.numberKey} title="6" onPress={() => number(6)} />
                    </View>
                    <View style={styles.row}>
                        <CalculatorButton style={styles.numberKey} title="1" onPress={() => number(1)} />
                        <CalculatorButton style={styles.numberKey} title="2" onPress={() => number(2)} />
                        <CalculatorButton style={styles.numberKey} title="3" onPress={() => number(3)} />
                    </View>
                    <View style={styles.row}>
                        <CalculatorButton style={styles.numberKey} title="." onPress={() => number('.')} />
                        <CalculatorButton style={styles.numberKey} title="0" onPress={() => number(0)} />
                        <CalculatorButton style={styles.enterKey} title="enter" onPress={enter} />
                    </View>
                </View>
                <View style={styles.operatorsContainer}>
                    <View style={{ flexDirection: 'row' }}>
                        <CalculatorButton style={styles.operatorKey} title="^" onPress={() => operator('^')} />
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <CalculatorButton style={styles.operatorKey} title="/" onPress={() => operator('/')} />
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <CalculatorButton style={styles.operatorKey} title="x" onPress={() => operator('*')} />
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <CalculatorButton style={styles.operatorKey} title="+" onPress={() => operator('+')} />
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <CalculatorButton style={styles.operatorKey} title="-" onPress={() => operator('-')} />
                    </View>
                </View>
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'center',
    },
    registryRow: {
        flexDirection: 'row',
        backgroundColor: '#222831',
        paddingTop: 5,
        paddingBottom: 5,
        height: 30,
    },
    registry: {
        flex: 1,
    },
    registryText: {
        color: '#f2a365',
        paddingLeft: 5,
        paddingRight: 5,
    },
    numberKey: {
        color: '#ececec',
        backgroundColor: '#222831',
        alignItems: 'center',
        justifyContent: 'center',
        width: (screenWidth / 4) - 10,
        height: 80 - 10,
    },
    operatorKey: {
        color: '#f2a365',
        backgroundColor: '#30475e',
        alignItems: 'center',
        justifyContent: 'center',
        width: (screenWidth / 4) - 10,
        height: 80 - 10,
    },
    enterKey: {
        color: '#222831',
        backgroundColor: '#f2a365',
        alignItems: 'center',
        justifyContent: 'center',
        width: (screenWidth / 4) - 10,
        height: 80 - 10,
    },
    clearKey: {
        color: '#f2a365',
        backgroundColor: '#30475e',
        alignItems: 'center',
        justifyContent: 'center',
        width: (screenWidth / 4) - 10,
        height: 80 - 10,
    },
    radianKey: {
        color: '#222831',
        backgroundColor: '#f2a365',
        alignItems: 'center',
        justifyContent: 'center',
        width: (screenWidth / 3) - 10,
        height: 40 - 10,
    },
    display: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
    },
    displayText: {
        fontSize: 40
    },
    numPadContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'stretch',
    },
    numPad: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        justifyContent: 'flex-end',
    },
    row: {
        flexDirection: 'row',
    },
    operatorsContainer: {
        flexDirection: 'column',
        justifyContent: 'flex-end',
    },
});
