import React, { useRef, useState, useEffect } from 'react';
import { NavigationContainer, DefaultTheme, DarkTheme, useLinking } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Platform, StatusBar, StyleSheet, Text, View } from 'react-native';
import { AppearanceProvider, useColorScheme } from 'react-native-appearance';
import * as SecureStore from 'expo-secure-store';

// Screens
import { Linking, SplashScreen } from 'expo';
import { CalculatorScreen } from './screens/CalculatorScreen';

const Stack = createStackNavigator();
const AuthContext = React.createContext();

const App = (props) => {
    const [accessToken, setAccessToken] = useState();
    const [isLoadingComplete, setLoadingComplete] = useState(false);
    const [initialNavigationState, setInitialNavigationState] = useState();
    const containerRef = useRef();
    const { getInitialState } = useLinking(containerRef, {
        prefixes: [Linking.makeUrl('/')],
        config: {
            Root: {
                path: 'root',
                screens: {
                    Calculator: 'calculator',
                },
            },
        },
    });
    const colorScheme = useColorScheme();

    // Load any resources or data needed prior to rendering the app.
    useEffect(() => {
        async function load() {
            try {
                // Do not allow dismissing of the splash screen.
                SplashScreen.preventAutoHide();

                // Load our navigation state.
                setInitialNavigationState(await getInitialState());
            } catch (e) {
                console.warn(e);
            } finally {
                setLoadingComplete(true);
                SplashScreen.hide();
            }
        };

        load();
    }, []);

    if (!isLoadingComplete && !props.skipLoadingScreen) {
        return null;
    }

    return (
      <View style={styles.container}>
        {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
        <NavigationContainer ref={containerRef} initialState={initialNavigationState} theme={colorScheme === 'light' ? DarkTheme : DefaultTheme}>
          <Stack.Navigator>
            <Stack.Screen name="RPN Calculator" component={CalculatorScreen} />
          </Stack.Navigator>
        </NavigationContainer>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const AppContainer = (props) => {
  return (
    <AppearanceProvider>
      <App {...props} />
    </AppearanceProvider>
  );
};

export default AppContainer;
